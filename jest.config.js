module.exports = {
    preset: 'ts-jest',
    testEnvironment: 'node',
    roots: ['<rootDir>/src'], // Adjust if your source files are located elsewhere
    transform: {
        '^.+\\.ts$': 'ts-jest',
    },
    testRegex: '(/__tests__/.*|(\\.|/)(test|spec))\\.ts$', // Adjust if your test files have a different pattern
    moduleFileExtensions: ['ts', 'js', 'json', 'node'],
};
