import VideoService from './video.service';
import FfmpegWrapper from '../wrappers/ffmpeg.wrapper';
import RandomStringService from './random-string.service';

jest.mock('../wrappers/ffmpeg.wrapper');
jest.mock('./random-string.service');

describe('VideoService', () => {
    let videoService: VideoService;
    let ffmpegWrapper: FfmpegWrapper;
    let randomStringService: RandomStringService;

    beforeEach(() => {
        ffmpegWrapper = new FfmpegWrapper();
        randomStringService = new RandomStringService();
        videoService = new VideoService(ffmpegWrapper, randomStringService);
    });

    describe('getVideoMetadata', () => {
        it('should fetch video metadata', async () => {
            const mockMetadata = { duration: 120, format: 'mp4', size: 1024 };
            ffmpegWrapper.getVideoMetadata = jest.fn().mockResolvedValue(mockMetadata);

            const filePath = 'test.mp4';
            const metadata = await videoService.getVideoMetadata(filePath);

            expect(ffmpegWrapper.getVideoMetadata).toHaveBeenCalledWith(filePath);
            expect(metadata).toEqual(mockMetadata);
        });
    });

    describe('getVideoThumbnail', () => {
        it('should generate a video thumbnail', async () => {
            const filePath = 'test.mp4';
            const randomFileName = 'randomstring123';
            const expectedThumbnailPath = './assets/thumbnails/randomstring123.jpg';

            randomStringService.generate = jest.fn().mockReturnValue(randomFileName);
            ffmpegWrapper.generateThumbnail = jest.fn().mockResolvedValue(expectedThumbnailPath);

            const thumbnailPath = await videoService.getVideoThumbnail(filePath);

            expect(randomStringService.generate).toHaveBeenCalledWith(16);
            expect(ffmpegWrapper.generateThumbnail).toHaveBeenCalledWith(filePath, randomFileName, './assets/thumbnails', '800x800');
            expect(thumbnailPath).toEqual(expectedThumbnailPath);
        });
    });
});
