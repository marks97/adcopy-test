import { statSync } from "fs";

import VideoMetadata from "../interfaces/video-metadata.interface";
import FfmpegWrapper from "../wrappers/ffmpeg.wrapper";
import RandomStringService from "./random-string.service";

const THUMBNAIL_PATH = "./assets/thumbnails";
const THUMBNAIL_SIZE = "800x800";

export default class VideoService {
    private ffmpegWrapper: FfmpegWrapper;
    private randomStringService: RandomStringService;

    constructor(ffmpegWrapper: FfmpegWrapper, randomStringService: RandomStringService) {
        this.ffmpegWrapper = ffmpegWrapper;
        this.randomStringService = randomStringService;
    }

    public async getVideoMetadata(filePath: string): Promise<VideoMetadata> {
        return this.ffmpegWrapper.getVideoMetadata(filePath);
    }

    public async getVideoThumbnail(filePath: string): Promise<string> {
        const randomFileName = this.randomStringService.generate(16);

        return this.ffmpegWrapper.generateThumbnail(
            filePath,
            randomFileName,
            THUMBNAIL_PATH,
            THUMBNAIL_SIZE
        );
    }

}
