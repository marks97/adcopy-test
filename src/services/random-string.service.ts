import { generateRandomString } from "ts-randomstring/lib"

export default class RandomStringService {
    /**
     * Generates a random string of a given length.
     *
     * @param length The length of the string to generate.
     *
     * @returns A random string of the specified length.
     */
    public generate(length: number): string {
        return generateRandomString({ length });
    }
}
