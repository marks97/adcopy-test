import ffmpeg from "fluent-ffmpeg";
import { statSync } from "fs";

import VideoMetadata from "../interfaces/video-metadata.interface";

export default class FfmpegWrapper {
    /**
     * Gets metadata for a video file
     *
     * @param filePath The path to the video file.
     *
     * @returns The video metadata.
     */
    public async getVideoMetadata(filePath: string): Promise<VideoMetadata> {
        const metadata = await this.getMetadata(filePath);

        const [stream] = metadata.streams;
        const {
            width,
            height,
            codec_name: codecName,
            display_aspect_ratio: displayAspectRatio,
            avg_frame_rate: avgFrameRate,
            duration, bit_rate: bitRate
        } = stream;

        const size = statSync(filePath).size;
        const creationTime = stream.tags ? stream.tags.creation_time : '';

        return {
            width,
            height,
            codecName,
            displayAspectRatio,
            avgFrameRate,
            duration,
            bitRate,
            size,
            creationTime,
        };
    }

    /**
     * Generates a thumbnail for a video file using async/await.
     *
     * @param filePath The path to the video file.
     * @param thumbnailPath The path where the thumbnail should be saved.
     * @param filename The name of the thumbnail file.
     * @param thumbnailSize The size of the thumbnail.
     *
     * @returns The path of the generated thumbnail.
     */
    public async generateThumbnail(filePath: string, filename: string, thumbnailPath: string, thumbnailSize: string): Promise<string> {
        const outputPath = `${thumbnailPath}/${filename}`;

        await this.createThumbnail(filePath, filename, thumbnailPath, thumbnailSize);

        return outputPath;
    }

    /**
     * A helper function to wrap the thumbnail creation process in a promise.
     *
     * @param filePath The path to the video file.
     * @param thumbnailPath The path where the thumbnail should be saved.
     * @param filename The name of the thumbnail file.
     * @param thumbnailSize The size of the thumbnail.
     */
    private createThumbnail(filePath: string, filename: string, thumbnailPath: string, thumbnailSize: string): Promise<void> {
        return new Promise((resolve, reject) => {
            ffmpeg(filePath)
                .on('end', () => {
                    resolve();
                })
                .on('error', (error: any) => {
                    reject(error);
                })
                .screenshots({
                    count: 1,
                    folder: thumbnailPath,
                    size: thumbnailSize,
                    filename: filename
                });
        });
    }


    /**
     * A helper function to promisify the ffprobe method.
     *
     * @param filePath The path to the video file.
     *
     * @returns The ffprobe metadata.
     */
    private getMetadata(filePath: string): Promise<any> {
        return new Promise((resolve, reject) => {
            ffmpeg.ffprobe(filePath, (err, metadata) => {
                if (err) {
                    reject(err);
                } else {
                    resolve(metadata);
                }
            });
        });
    }
}
