import FfmpegWrapper from "./wrappers/ffmpeg.wrapper";
import RandomStringService from "./services/random-string.service";
import VideoService from "./services/video.service";

const ffmpegWrapper = new FfmpegWrapper();
const randomStringService = new RandomStringService();
const videoService = new VideoService(ffmpegWrapper, randomStringService);

const BASE_VIDEO_PATH = './assets/videos';

async function main(filePath: string) {
    try {
        const metadata = await videoService.getVideoMetadata(filePath);
        const thumbnailPath = await videoService.getVideoThumbnail(filePath);

        return {
            metadata,
            thumbnailPath
        }
    } catch (e) {
        console.error(e);
    }
}

const [,, videoFilename] = process.argv;

if (videoFilename) {
    main(`${BASE_VIDEO_PATH}/${videoFilename}`).then(result => {
        console.log(result);
    }).catch(error => {
        console.error('Error running main function:', error);
    });
}

export default main;
