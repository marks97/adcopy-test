import fs from 'fs';
import path from 'path';
import main from "./index";

describe('e2e test for processing all videos', () => {
    const videosPath = './assets/videos';

    it('should process each video file successfully', async () => {
        const videoFiles = fs.readdirSync(videosPath);

        for (const file of videoFiles) {
            const filePath = path.join(videosPath, file);
            const result = await main(filePath);

            expect(result).toHaveProperty('metadata');
            // @ts-ignore
            expect(result.metadata).not.toBeNull();
            expect(result).toHaveProperty('thumbnailPath');
            // @ts-ignore
            expect(result.thumbnailPath).not.toBeNull();
        }
    });
});
