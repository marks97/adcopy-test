export default interface VideoMetadata {
    width: number;
    height: number;
    codecName: string;
    displayAspectRatio: string;
    avgFrameRate: string;
    duration: string;
    bitRate: string;
    size: number;
    creationTime: string;
}
