# Adcopy Test

## Introduction
`adcopytest` is a NodeJS application that leverages the `fluent-ffmpeg` package to process video files. It is designed to extract metadata and generate thumbnails from videos, especially handling specific cases where videos fail to be processed due to an unknown error.

## Features
- Extracts crucial metadata from video files: width, height, codec_name, display_aspect_ratio, avg_frame_rate, duration, bit_rate, size, and creation_time.
- Generates a thumbnail (800x800 PNG) from the video.

## Installation
Clone the repository and install the dependencies:
```bash
git clone git@gitlab.com:marks97/adcopy-test.git
cd adcopytest
npm install
```

## Usage

1. Place your video files in a specified directory (e.g., `/videos`).
2. Start the application by running:

```bash
npm start
```

## Development and Testing

- To run the unit tests that are included in the project, execute the following command in your terminal:

```bash
npm test
```
